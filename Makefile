IDIR =./include
CC=gcc
CFLAGS=-c -I$(IDIR)
DEPS=point.h

all: distancia

distancia: main.o point.o ./include/point.h
	$(CC) -o distancia main.o point.o -I$(IDIR) -lm

main.o: main.c
	$(CC) $(CFLAGS) main.c

point.o: point.c
	$(CC) $(CFLAGS) point.c

 
clean:
	rm *o distancia
	
