#include<point.h>

int main() {
	Point p1;
	Point p2;
	
	printf("Escriba las coordenadas de los puntos \nPunto 1 \nx: ");
	scanf("%f" , &p1.x);
	printf("y: ");
	scanf("%f" , &p1.y);
	printf("z: ");
	scanf("%f" , &p1.z);
	printf("Punto 2 \nx: ");
	scanf("%f" , &p2.x);
	printf("y: ");
	scanf("%f" , &p2.y);
	printf("z: ");
	scanf("%f" , &p2.z);

	float distancia;
	Point pmedio;
	
	distancia = dist_Eucl(p1,p2);
	pmedio = punto_medio(p1,p2);
	
	printf("La distancia es %f metros\n", distancia);
	printf("El punto medio es: x = %0.3f, y = %0.3f, z = %0.3f \n",pmedio.x,pmedio.y,pmedio.z);
	return (0);
	
}
