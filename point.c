#include<point.h>


float dist_Eucl(Point p1, Point p2) {
	float suma = pow(p1.x - p2.x, 2)+pow(p1.y - p2.y, 2)+pow(p1.z - p2.z, 2);
	return sqrt(suma);
}

Point punto_medio(Point p1, Point p2){
	float x = (p1.x + p2.x)/2.0;
	float y = (p1.y + p2.y)/2.0;
	float z = (p1.z + p2.z)/2.0;

	Point punto;
	punto.x=x;
	punto.y=y;
	punto.z=z;
	
	return punto;
}
